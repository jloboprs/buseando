module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);

    var pkg = grunt.file.readJSON('package.json');

    grunt.initConfig({
        grunt: pkg.grunt,
        watch: {
            express: {
                files: pkg.grunt.reloadFiles,
                tasks: ['express:default'],
                options: {
                    livereload: true,
                    spawn: false
                }
            },
            livereload: {
                files: pkg.grunt.reloadFiles,
                options: {
                    livereload: true
                }
            },
            gruntfile: {
                files: ['Gruntfile.js']
            }
        },
        express: {
            options: {
                port: 3000
            },
            default: {
                options: {
                    debug: true,
                    script: 'app/bin/dev'
                }
            },
            build: {
                options: {
                    background: false,
                    script: 'app/bin/www'
                }
            }
        },
        open: {
            server: {
                path: 'http://127.0.0.1:<%=express.options.port%>'
            }
        },
        clean: ['.tmp', '<%=grunt.dist%>/{,*/}*'],
        copy: {
            main: {
                files: [{
                    expand: true,
                    cwd: pkg.grunt.client,
                    dest: pkg.grunt.dist,
                    src: ['views/**', 'font/**']
                }, {
                    expand: true,
                    cwd: 'bower_components/fontello',
                    dest: pkg.grunt.dist,
                    src: 'font/**'
                }]
            }
        },
        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%=grunt.client%>/images',
                    src: '{,*/}*.{png,jpg,jpeg,gif}',
                    dest: '<%=grunt.dist%>/images'
                }]
            }
        },
        useminPrepare: {
            html: ['<%=grunt.client%>/views/layout.html', '<%=grunt.client%>/views/root.html',
                '<%=grunt.client%>/views/pasajes.html', '<%=grunt.client%>/views/asientos.html' ],
            options: {
                dest: pkg.grunt.dist,
                root: pkg.grunt.client,
                flow: {
                    html: {
                        steps: {
                            js: ['concat', 'uglifyjs'],
                            css: ['cssmin']
                        },
                        post: {}
                    }
                }
            }
        },
        // Performs rewrites based on filerev and the useminPrepare configuration
        usemin: {
            html: ['<%=grunt.dist%>/{,*/}*.html'],
            css: ['<%=grunt.dist%>/styles/{,*/}*.css'],
            options: {
                assetsDirs: [pkg.grunt.dist, '<%=grunt.dist%>/images']
            }
        },
        filerev: {
            dist: {
                src: [
                    '<%=grunt.dist%>/scripts/{,*/}*.js',
                    '<%=grunt.dist%>/styles/{,*/}*.css',
                    '<%=grunt.dist%>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
                    '<%=grunt.dist%>/styles/font/*'
                ]
            }
        },
        htmlmin: {
            dist: {
                options: {
                    collapseWhitespace: true,
                    conservativeCollapse: true,
                    collapseBooleanAttributes: true,
                    removeCommentsFromCDATA: true,
                    removeOptionalTags: false
                },
                files: [{
                    expand: true,
                    cwd: pkg.grunt.dist,
                    src: ['views/{,*/}*.html'],
                    dest: pkg.grunt.dist
                }]
            }
        }
    });

    grunt.registerTask('default', [
        'express:default',
        'open',
        'watch'
    ]);
    grunt.registerTask('build', [
        'clean',
        'copy',
        'imagemin',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'filerev',
        'usemin',
        'htmlmin',
        'open',
        'express:build'
    ]);
};