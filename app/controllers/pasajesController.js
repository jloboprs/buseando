'use strict';

var q = require('q'),
    util = require('util'),
    under = require('underscore'),
    nodos = require('nodos'),
    sugerencia = require('../business/sugerencia'),
    utils = require('../lib/utils'),
    resumenPasajes = require('../models/resumenPasajes');

var botRedirect = function botRedirect(req, res, next){
    var bots = [/googlebot/ig, /bingbot/ig];
    var esBot = under(bots).some(function(bot) {
        return bot.test(req.headers['user-agent']);
    });

    if (!esBot){
        return next();
    }

    var origen = req.params.origen || req.query.origen;
    var destino = req.params.destino || req.query.destino;

    res.redirect(301, util.format('%s://%s/pasajes/%s/%s', req.protocol, req.headers.host, origen, destino));
};

var obtenerContexto = function obtenerContexto(req, res, next){
    if (req.subdomains.length !== 1){
        return next();
    }

    req.contexto = { operador: req.subdomains[0] };

    return next();
};

var generarTramo = function generarTramo(req, res, next) {
    req.tramo = utils.homologarTramo(req.params);

    req.tramo = !req.tramo.esValido()
        ? utils.homologarTramo(req.query)
        : req.tramo;

    if (!req.tramo.esValido()){
        return next(new Error('Tramo invalido.'));
    }

    if (!req.tramo.salida){
        req.tramo.salida = sugerencia.obtenerFinDeSemana();
    }

    return next();
};

var comprimirTramo = function comprimirTramo(req, res, next) {
    utils.comprimir(req.tramo).then(function(tramoComprimido){
        req.tramo = Object.create(req.tramo);
        req.tramo.compresion = tramoComprimido;

        next();
    }).fail(next);
};

var generarSugerencias = function generarSugerencias(req, res, next) {
    res.sugerencias = sugerencia.obtenerBusquedasfrecuentes(req.tramo);

    return next();
};

var buscarPasajes = function buscarPasajes(req, res, next) {
    nodos.buscarPasajes(req.tramo).then(function (pasajes) {
        res.pasajes = pasajes;
        next();

    }).fail(next);
};

var comprimirPasajes = function comprimirPasajes(req, res, next) {
    var promesasPasajes = under(res.pasajes).map(function(pasaje){
        return utils.comprimir(pasaje).then(function(pasajeComprimido){
            pasaje = Object.create(pasaje);
            pasaje.compresion = pasajeComprimido;
            return pasaje;
        });
    });

    q.all(promesasPasajes).then(function(pasajes){
        res.pasajes = pasajes;
        next();

    }).fail(next);
};

var generarResumenPasajes = function generarResumenPasajes(req, res, next) {
    res.resumen = resumenPasajes(res.pasajes);
    next();
};

var renderizarPasajes = function renderizarPasajes(req, res) {
    res.render('pasajes', {
        tramo: req.tramo,
        pasajes: res.pasajes,
        sugerencias: res.sugerencias,
        resumen: res.resumen,
        contexto: req.contexto
    });
};

var tratarError = function tratarError(error ,req, res, next) {
    res.send(error.toString());
};

module.exports = {
    botRedirect: botRedirect,
    obtenerContexto: obtenerContexto,
    generarTramo: generarTramo,
    comprimirTramo: comprimirTramo,
    generarSugerencias: generarSugerencias,
    buscarPasajes: buscarPasajes,
    comprimirPasajes: comprimirPasajes,
    generarResumenPasajes: generarResumenPasajes,
    renderizarPasajes: renderizarPasajes,
    tratarError: tratarError
};