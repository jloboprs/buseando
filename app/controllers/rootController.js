'use strict';
var geoip = require('geoip-lite'),
    sugerencia = require('../business/sugerencia');

var obtenerGeo = function obtenerGeo(req, res, next) {
    var ip = req.ip === '127.0.0.1' ? '190.157.200.188' : req.ip;

    var geo = geoip.lookup(ip);

    req.contexto = {
        ciudad: geo ? geo.city : undefined,
        headers: req.headers
    };

    next();
};

var generarSugerencia = function generarSugerencia(req, res, next) {
    res.sugerencia = sugerencia.obtenerTramoRandom(req.tramo);

    return next();
};

var enviarPaginaPrincipal = function enviarPaginaPrincipal(req, res) {
    res.render('root', { contexto: req.contexto, sugerencia: res.sugerencia });
};

module.exports = {
    obtenerGeo: obtenerGeo,
    generarSugerencia: generarSugerencia,
    enviarPaginaPrincipal: enviarPaginaPrincipal
};