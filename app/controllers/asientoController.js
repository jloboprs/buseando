var q = require('q'),
    asiento = require('../models/asiento'),
    utils = require('../lib/utils');

var obtenerParametros = function obtenerParametros(req, res, next){
    var promesas = [ utils.descomprimir(req.params.tramo), utils.descomprimir(req.params.pasaje)];

    q.all(promesas).spread(function(tramo, pasaje){
        req.tramo = tramo;
        req.pasaje = pasaje;

        return next();
    }).fail(next).done();
};

var obtenerAsientos = function obtenerAsientos(req, res, next){
    var pasillo = asiento.crearForma({tipo:'pasillo'});
    var ocupado = asiento.crearAsiento({estado:'ocupado', codigo: 0});

    res.asientos = [
        [ocupado, ocupado, pasillo, ocupado, ocupado],
        [ocupado, ocupado, pasillo, ocupado, ocupado]
    ];

    for (var i = 2; i < 14; i++) {
        var fila = i * 4;
        res.asientos.push([
            asiento.crearAsiento({estado:'disponible', codigo: fila + 1, precio: 10 * i}),
            asiento.crearAsiento({estado:'disponible', codigo: fila + 2, precio: 10 * i}),
            pasillo,
            asiento.crearAsiento({estado:'disponible', codigo: fila + 3, precio: 5 * i}),
            asiento.crearAsiento({estado:'disponible', codigo: fila + 4, precio: 5 * i})
        ]);
    }

    return next();
};

var renderizarAsientos = function renderizarAsientos(req, res){
    res.render('asientos', {
        tramo: req.tramo,
        pasaje: req.pasaje,
        asientos: res.asientos
    });
};

module.exports = {
    obtenerParametros: obtenerParametros,
    obtenerAsientos: obtenerAsientos,
    renderizarAsientos: renderizarAsientos
};