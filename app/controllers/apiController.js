'use strict';

var path = require('path');

var enviarLugares = function enviarLugares(req, res) {
    res.sendFile(path.resolve(path.join(__dirname, '../client/json/lugares.json')));
};

module.exports = { enviarLugares: enviarLugares };