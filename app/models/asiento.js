var under = require('underscore');
var util =  require('util');

var asiento = function asiento(valores){
    var toString = function toString(){
        if (asientoBase.estado === 'disponible') {
            return util.format('<button type="button" id="asiento%s" class="%s" data-precio="%d" data-codigo="%s">%s</button>',
                asientoBase.codigo, asientoBase.estado, asientoBase.precio, asientoBase.codigo, asientoBase.codigo);
        }else {
            return util.format('<span id="asiento%s" class="%s" data-precio="%d" data-codigo="%s">%s</span>',
                asientoBase.codigo, asientoBase.estado, asientoBase.precio, asientoBase.codigo, asientoBase.codigo);
        }
    };

    var asientoBase = {
        tipo: 'asiento',
        estado: 'disponible',
        servicio: '',
        codigo: '',
        precio: 0,
        toString: toString,
        constructor: asiento
    };

    return under.extend(asientoBase, valores);
};

var forma = function forma(valores){
    var toString = function toString(){
        return util.format('<span class="%s"></span>', formaBase.tipo);
    };

    var formaBase = {
        tipo:'wr',
        toString: toString,
        constructor: forma
    };

    return under.extend(formaBase, valores);
};

module.exports = { crearForma: forma, crearAsiento: asiento };