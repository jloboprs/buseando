var under = require('underscore');

var resumenPasajes = function resumenPasajes(pasajes) {
    var pasaje = under.first(pasajes);
    if (pasaje === undefined) {
        return pasaje;
    }

    var duracion = pasaje.duracion();
    var resumen = {
        operadores: [],
        precio: { min: pasaje.precio(), max: pasaje.precio() },
        duracion: { min: duracion, max: duracion },
        sillas: { min: pasaje.sillas, max: pasaje.sillas },
        salida: { min: pasaje.salida, max: pasaje.salida },
        constructor: resumenPasajes
    };

    under.reduce(pasajes, function(resumen, pasaje){
        if (resumen.operadores.indexOf(pasaje.operador.nombre) === -1){
            resumen.operadores.push(pasaje.operador.nombre);
        }

        if (resumen.precio.max < pasaje.precio()){
            resumen.precio.max = pasaje.precio();
        }

        if (resumen.precio.min > pasaje.precio()){
            resumen.precio.min = pasaje.precio();
        }

        if (resumen.salida.max < pasaje.salida){
            resumen.salida.max = pasaje.salida;
        }

        if (resumen.salida.min > pasaje.salida){
            resumen.salida.min = pasaje.salida;
        }

        var duracion = pasaje.duracion();
        if (duracion && (!resumen.duracion.max || resumen.duracion.max.total < duracion.total)){
            resumen.duracionMax = duracion;
        }

        if (duracion && (!resumen.duracion.min || resumen.duracion.min.total > duracion.total)){
            resumen.duracion.min = duracion;
        }

        if (pasaje.sillas && (!resumen.sillas.max || resumen.sillas.max < pasaje.sillas)){
            resumen.sillas.max = pasaje.sillas;
        }

        if (pasaje.sillas && (!resumen.sillas.min || resumen.sillas.min > pasaje.sillas)){
            resumen.sillas.min = pasaje.sillas;
        }

        return resumen;
    }, resumen);

    return resumen;
};

module.exports = resumenPasajes;