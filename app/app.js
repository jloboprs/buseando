'use strict';

var swig = require('swig');
var path = require('path');
var express = require('express');
var under = require('underscore');
var compression = require('compression');
var routes = require('./routes');
var swigExtension = require('./lib/swigExtension');
var redireccion = require('./lib/redireccion');
//var favicon = require('static-favicon');
//var cookieParser = require('cookie-parser');
//var bodyParser = require('body-parser');

var app = express();
app.static = express.static;
swigExtension(swig);

app.use(compression());
app.use(redireccion.www);
app.enable('trust proxy');
app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('subdomain offset', 2);
app.all('/', redireccion.subdominio);

under(routes).each(function(rout) {
    app.use(rout);
});

module.exports = app;