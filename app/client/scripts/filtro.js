(function(){
    var filtro = $('#filtro'),
        filtroSillas = $( "#filtroSillas"),
        filtroDuracion = $( "#filtroDuracion"),
        filtroSalida = $( "#filtroSalida"),
        filtroPrecio = $( "#filtroPrecio"),
        filtroSillasValor = $('#filtroSillasValor'),
        filtroDuracionValor = $('#filtroDuracionValor'),
        filtroSalidaRango = $('#filtroSalidaRango'),
        filtroPrecioValor = $('#filtroPrecioValor'),
        filtroOperadores = $('#filtroOperadores input[type=checkbox]'),
        resumen = filtro.data('resumen'),
        listaPasajes;

    var formatearNumero = function formatearNumero(number) {
        return number.toString().replace(/\d(?=(\d{3})+$)/g, '$&,');
    };

    var formatearHora = function formatearHora(hora) {
        return hora > 12
            ? hora - 12 + 'PM'
            : hora + 'AM';
    };

    var formatearFiltroSalida = function formatearFiltroSalida(hora) {
        return hora < 10
            ? '0' + hora + '00'
            : hora + '00';
    };

    var filtrarSalida = function filtrarSalida(pasaje, filtro){
        return pasaje.salida  >= formatearFiltroSalida(filtro.salida.min)
            && pasaje.salida  <= formatearFiltroSalida(filtro.salida.max);
    };

    var filtrarDuracion = function filtrarDuracion(pasaje, filtro){
        return pasaje.duracionFiltro <= filtro.duracion;
    };

    var filtrarSillas = function filtrarSillas(pasaje, filtro){
        return pasaje.sillas <= filtro.sillas;
    };

    var filtrarOperador = function filtrarOperador(pasaje, filtro){
        return filtro.operadores.indexOf(pasaje.operador) > -1;
    };

    var filtrarPrecio = function filtrarPrecio(pasaje, filtro){
        return pasaje.precio >= filtro.precio.min
            && pasaje.precio <= filtro.precio.max;
    };

    var filtrar = function filtrar(){
        var filtro = obtenerFiltro();

        listaPasajes.filter(function(pasajeDom) {
            var pasaje = pasajeDom.values();

            return filtrarSillas(pasaje, filtro)
                && filtrarPrecio(pasaje, filtro)
                && filtrarSalida(pasaje, filtro)
                && filtrarOperador(pasaje, filtro)
                && filtrarDuracion(pasaje, filtro);
        });
    };

    var obtenerValorFiltro = function obtenerValorFiltro(filtro) {
        return filtro.slider('value');
    };

    var obtenerRangoFiltro = function obtenerRangoFiltro(filtro) {
        var rango = filtro.slider('values');

        return { min: rango[0], max: rango[1] };
    };

    var obtenerFiltro = function obtenerFiltro() {
        var operadores = filtroOperadores.filter(':checked')
            .map(function() { return $(this).val(); })
            .toArray();

        return {
            operadores: operadores,
            precio: obtenerRangoFiltro(filtroPrecio),
            salida: obtenerRangoFiltro(filtroSalida),
            duracion: obtenerValorFiltro(filtroDuracion),
            sillas: obtenerValorFiltro(filtroSillas)
        };
    };

    cambioFiltro = function cambioFiltro(elemento, formato){
        return function(event, ui) {
            if (ui.values){
                var min = ui.values[0], max = ui.values[1];

                if (formato) {
                    min = formato(min);
                    max = formato(max);
                }

                elemento.html(min + '-' + max);
            }else{
                var valor = formato
                    ? formato(ui.value)
                    : ui.value;

                elemento.html(valor);
            }

            filtrar();
        };
    };

    var init = function() {
        if (!resumen) {
            return;
        }

        listaPasajes = new List('pasajes', { valueNames: ['calidad', 'oferta', 'operador', 'precio', 'salida', 'llegada',
            'duracion', 'duracionFiltro','sillas'] });

        filtroOperadores.click(filtrar);

        filtroSalida.slider({
            range: true,
            min: 1,
            max: 24,
            values: [ 1, 24 ],
            stop: cambioFiltro(filtroSalidaRango, formatearHora),
            slide: cambioFiltro(filtroSalidaRango, formatearHora)
        });

        filtroPrecio.slider({
            range: true,
            step: 500,
            min: resumen.precio.min,
            max: resumen.precio.max,
            values: [ resumen.precio.min, resumen.precio.max ],
            stop: cambioFiltro(filtroPrecioValor, formatearNumero),
            slide: cambioFiltro(filtroPrecioValor, formatearNumero)
        });

        if (resumen.sillas.min) {
            filtroSillas.slider({
                range: 'min',
                min: resumen.sillas.min,
                max: resumen.sillas.max,
                value: resumen.sillas.max,
                stop: cambioFiltro(filtroSillasValor),
                slide: cambioFiltro(filtroSillasValor)
            });
        }

        if (resumen.duracion.min) {
            var duracionMin = Math.floor(resumen.duracion.min.total),
                duracionMax = Math.floor(resumen.duracion.max.total);

            filtroDuracion.slider({
                range: 'min',
                min: duracionMin,
                max: duracionMax,
                value: duracionMax,
                stop: cambioFiltro(filtroDuracionValor),
                slide: cambioFiltro(filtroDuracionValor)
            });
        }

        filtrar();
    };

    init();
})();