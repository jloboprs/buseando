$(document).ready(function() {
    var control = {
        menuOrder: $('#menu_orden'),
        filtro: $('#filtro'),
        pasajes: $('#pasajes'),
        listaPasajes: $("#lista-pasajes"),
        botonesOrden: $('#menu_orden button')
    };

    var getTopOffset = function getTopOffset() {
        return control.pasajes.offset().top;
    };

    var metodos = {
        configurarAffix: function configurarAffix() {
            control.menuOrder.affix({ offset: { top: getTopOffset } });
            control.filtro.affix({ offset: { top: getTopOffset } });
        },
        configurarEventos: function configurarEventos(){
            control.menuOrder.on('affixed.bs.affix', function () {
                    control.listaPasajes.addClass('before-affix');
                })
                .on('affixed-top.bs.affix', function () {
                    control.listaPasajes.removeClass('before-affix');
                });

            control.botonesOrden.click(function() {
                if (control.menuOrder.hasClass('affix')){
                    $('html,body').animate({scrollTop: getTopOffset()}, 'fast');
                }
            });
        },
        init: function init(){
            metodos.configurarAffix();
            metodos.configurarEventos();
        }
    };

    metodos.init();
});