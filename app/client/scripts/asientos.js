(function() {
    var pasaje;
    var pasajeros = function pasajeros(dom){
        var pasajeroBase;
        var cargaPasajero = cargaPasajeros();

        var agregarPasajeroLista = function agregarPasajeroLista(asiento){
            pasajeroBase.lista[asiento.codigo] = asiento;
        };

        var quitarPasajeroLista = function AgregarPasajeroLista(asiento){
            delete pasajeroBase.lista[asiento.codigo];
        };

        var formatPasajero = function formatPasajero(pasajero){
            var nombre = pasajero.nombre || 'Sin pasajero';

            return ['<tr id="pasajero' ,pasajero.codigo ,'"><td>', pasajero.codigo, '</td><td id="nombrePasajero', pasajero.codigo, '">',
                nombre, '</td><td>$', pasajero.precio, '</td><td><button id="cargaPasajero', pasajero.codigo,
                '" class="btn btn-primary icon-user" role="button"></button></td></tr>'].join('');
        };

        var cargarPasajero = function cargarPasajero(pasajero) {
            var pasajeroLista = pasajeroBase.lista[pasajero.codigo];

            pasajeroLista.tipoDocumento = pasajero.tipoDocumento;
            pasajeroLista.numeroDocumento = pasajero.numeroDocumento;
            pasajeroLista.nombre = pasajero.nombre;
            $('#nombrePasajero'+pasajero.codigo).html(pasajero.nombre);
            $('#cargaPasajero'+pasajero.codigo).removeClass('icon-user')
                .addClass('icon-cancel');
        };

        var lanzarCargaPasajero = function lanzarCargaPasajero(asiento){
            cargaPasajero.lanzar(asiento.codigo, cargarPasajero);
        };

        var accionBotonPasajeros = function accionBotonPasajeros(asiento){
            return function() {
                var botonPasajero = $(this);
                quitarError();

                if (botonPasajero.hasClass('icon-user')) {
                    lanzarCargaPasajero(asiento);
                } else if (botonPasajero.hasClass('icon-cancel')) {
                    pasaje.quitarAsiento(asiento);
                }
            };
        };

        var agregarPasajeroAlDom = function agregarPasajeroAlDom(pasajero){
            dom.append(formatPasajero(pasajero));
            $('#cargaPasajero'+pasajero.codigo).click(accionBotonPasajeros(pasajero));
        };

        var agregarAsiento =function agregarAsiento(asiento){
            asiento = Object.create(asiento);
            agregarPasajeroLista(asiento);
            agregarPasajeroAlDom(asiento);

            toggleSeleccionAsiento($('#asiento'+asiento.codigo));
        };

        var quitarAsiento =function quitarAsiento(asiento){
            asiento = Object.create(asiento);

            quitarPasajeroLista(asiento);
            $('#pasajero'+asiento.codigo).remove();
            toggleSeleccionAsiento($('#asiento'+asiento.codigo));
        };

        var toggleSeleccionAsiento = function toggleSeleccionAsiento(asiento) {
            asiento.toggleClass('seleccionado');
            asiento.toggleClass('disponible');
        };

        var validarPasajeros = function validarPasajeros() {
            var indice, ok = true;
            for (indice in pasaje.lista) {
                var pasajero = pasaje.lista[indice];
                if (pasaje.lista.hasOwnProperty(indice)) {
                    if (!pasajero.tipoDocumento || !pasajero.numeroDocumento || !pasajero.nombre) {
                        ok = false;
                        $('#cargaPasajero' + pasajero.codigo).removeClass('btn-primary')
                            .addClass('btn-danger');
                    }
                }
            }

            return ok;
        };

        var quitarError = function quitarError() {
            var indice;
            for (indice in pasajeroBase.lista) {
                if (pasajeroBase.lista.hasOwnProperty(indice)) {
                    $('#cargaPasajero' + pasajeroBase.lista[indice].codigo).removeClass('btn-danger')
                        .addClass('btn-primary');
                }
            }
        };

        return pasajeroBase = {
            lista: {},
            agregarAsiento: agregarAsiento,
            quitarAsiento: quitarAsiento,
            validarPasajeros: validarPasajeros,
            constructor: pasajeros
        };
    };

    var cargaPasajeros = function cargaPasajeros(){
        var cargaPasajerosBase;
        var cargaPasajero = $('#cargaPasajero');
        var cargaPasajeroAsiento = $('#cargaPasajeroAsiento');
        var cargaPasajeroTipoDocumento = $('#cargaPasajeroTipoDocumento');
        var cargaPasajeroNombre = $('#cargaPasajeroNombre');
        var cargaPasajeroNumeroDocumento = $('#cargaPasajeroNumeroDocumento');
        var cargaPasajeroEtiquetaTipoDocumento = $('#cargaPasajeroEtiquetaTipoDocumento');

        var agregarPasajero = function agregarPasajero() {
            if (!cargaPasajerosBase.cargar){
                return;
            }

            var pasajero = {
                codigo: cargaPasajerosBase.codigo,
                nombre: cargaPasajeroNombre.val(),
                tipoDocumento: cargaPasajeroTipoDocumento.data('tipo-documento'),
                numeroDocumento: cargaPasajeroNumeroDocumento.val()
            };

            if  (validarFormulario(pasajero)){
                cargaPasajero.modal('hide');
                cargaPasajerosBase.cargar(pasajero);
            }
        };

        var cambiarTiposDocumento = function cambiarTiposDocumento() {
            var tipoDocumento = $(this).data('tipo-documento');
            cargaPasajeroTipoDocumento.data('tipo-documento', tipoDocumento);
            cargaPasajeroEtiquetaTipoDocumento.html(tipoDocumento);
        };

        var reiniciarValoresPorDefecto = function reiniciarValoresPorDefecto() {
            $('#cargaPasajeroListaTipoDocumento a:first').click();
            cargaPasajeroNombre.val('');
            cargaPasajeroNumeroDocumento.val('');
            removerError();
        };

        var lanzar = function lanzar(codigo, callback){
            cargaPasajerosBase.codigo = codigo;
            cargaPasajerosBase.cargar = callback;

            cargaPasajeroAsiento.html(cargaPasajerosBase.codigo);
            cargaPasajero.modal('show');
        };

        var removerError =  function removerError() {
            $('div.has-error', cargaPasajero).removeClass("has-error");
        };

        var validarFormulario = function validarFormulario (pasajero){
            var ok = true;

            var grupoSinComando = '[^\\s\'"<>\\{\\}\\[\\]\\(\\)]';

            var expresionNombre = new RegExp(util.format('^%s{3,}(\\s+%s{3,})+$', grupoSinComando, grupoSinComando));
            if (!pasajero.nombre || !expresionNombre.test(pasajero.nombre)) {
                cargaPasajeroNombre.parent().parent().addClass("has-error");
                ok = false;
            }

            var expresionNumeroDocumento = new RegExp(util.format('^%s+$', grupoSinComando));
            if (!pasajero.numeroDocumento || !expresionNumeroDocumento.test(pasajero.numeroDocumento)) {
                cargaPasajeroNumeroDocumento.parent().addClass("has-error");
                ok = false;
            }

            return ok;
        };

        var init = function init(){
            $('#cargaPasajeroAgregar').click(agregarPasajero);
            $('#cargaPasajeroListaTipoDocumento a').click(cambiarTiposDocumento);
            cargaPasajero.on('hidden.bs.modal', reiniciarValoresPorDefecto);
            cargaPasajeroNombre.on("focus", removerError);
            cargaPasajeroNumeroDocumento.on("focus", removerError);
        };

        init();

        return cargaPasajerosBase = {
            codigo: '00',
            cargar: undefined,
            lanzar: lanzar,
            constructor: cargaPasajeros
        };
    };

    var seleccionarAsiento = function seleccionarAsiento() {
        var asiento = $(this);

        if (asiento.hasClass('disponible')){
            pasaje.agregarAsiento(asiento.data());
        }else{
            pasaje.quitarAsiento(asiento.data());
        }
    };


    var comprarAsientos = function comprarAsientos() {
        pasaje.validarPasajeros();
    };
    var init = function init() {
        pasaje = pasajeros($('#pasajeros'));
        $('#asientos > div > button.disponible').click(seleccionarAsiento);
        $('#comprar').click(comprarAsientos);
    };

    $(document).ready(init);
})();