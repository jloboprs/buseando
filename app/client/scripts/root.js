'use strict';

(function(module) {
    var accentMap = { 'Á':'a', 'á':'a', 'À':'a', 'à':'a', 'Â':'a', 'â':'a', 'Ä':'a', 'ä':'a', 'Ã':'a', 'ã':'a', 'É':'e', 'é':'e', 'È':'e', 'è':'e', 'Ê':'e', 'ê':'e', 'Ë':'e', 'ë':'e', 'Ẽ':'e', 'ẽ':'e', 'Í':'i', 'í':'i', 'Ì':'i', 'ì':'i', 'Î':'i', 'î':'i', 'Ï':'i', 'ï':'i', 'Ĩ':'i', 'ĩ':'i', 'Ó':'o', 'ó':'o', 'Ò':'o', 'ò':'o', 'Ô':'o', 'ô':'o', 'Ö':'o', 'ö':'o', 'Õ':'o', 'õ':'o', 'Ú':'u', 'ú':'u', 'Ù':'u', 'ù':'u', 'Û':'u', 'û':'u', 'Ü':'u', 'ü':'u', 'Ũ':'u', 'ũ':'u' },
        control = {
            busqueda: $('#busqueda'),
            salida: $('#salida'),
            origen: $('#origen'),
            destino: $('#destino'),
            txtOrigen: $('#txtOrigen'),
            txtDestino: $('#txtDestino'),
            buscar: $('#buscar'),
            buscarSugerencia: $('#buscarSugerencia')
        };

    var metodo = {
        normalize: function normalize (a) {
            var b = null, c = null;

            if (!a) {
                return '';
            }

            for (b = '', c = 0; c < a.length; c++){
                b += accentMap[a.charAt(c)] || a.charAt(c);
            }

            return b;
        },
        filter: function filter (a) {
            var b = [],
                i = null;

            for (i in a) {
                var c = a[i];
                c.nombreBusqueda = metodo.normalize(c.completo).toUpperCase();
                b.push(c);
            }
            return b;
        },
        generarQuery: function generarQuery (a) {
            return Bloodhound.tokenizers.whitespace(metodo.normalize(a));
        },
        generarTokens: function generarTokens(campo) {
            return function split(lugar) {
                return lugar[campo].split(/[\s\(\)\-]+/);
            };
        },
        cambiarValorDelTramo: function cambiarValorDelTramo(self){
            return function cambiarValorDelTramoBind(evento, lugar) {
                self.val(lugar.codigo);
                self.data('completo',lugar.completo);
            };
        },
        borrarValorDelTramo: function borrarValorDelTramo(lugar){
            return function cambiarValorDelTramoBind() {
                if ($(this).val() !== lugar.data('completo')){
                    lugar.val('');
                }
            };
        },
        cambiarValorDelOrigenSegunContexto: function cambiarValorDelOrigenSegunContexto (lugares) {
            return function(){
                if (!module.contexto){
                    return null;
                }

                lugares.get(module.contexto.ciudad, function(lugaresPosibles) {
                    if (lugaresPosibles.length > 0){
                        control.origen.val(lugaresPosibles[0].codigo);
                        control.origen.data('completo',lugaresPosibles[0].completo);
                        control.txtOrigen.val(lugaresPosibles[0].completo);
                    }
                });
            };
        },
        configurarTypeahead: function configurarTypeahead() {
            var confBloodhound = {
                limit: 10,
                prefetch: { url: "/api/lugares?2asb", filter: metodo.filter },
                datumTokenizer: metodo.generarTokens("nombreBusqueda"),
                queryTokenizer: metodo.generarQuery
            };

            var lugares = new Bloodhound(confBloodhound);
            lugares.initialize().done(metodo.cambiarValorDelOrigenSegunContexto(lugares));

            var confTypeahead = { name: "lugares", displayKey: "completo", source: lugares.ttAdapter()};
            control.txtOrigen.typeahead(null, confTypeahead);
            control.txtDestino.typeahead(null, confTypeahead);

            control.txtOrigen.on("typeahead:selected", metodo.cambiarValorDelTramo(control.origen));
            control.txtOrigen.on("typeahead:autocompleted", metodo.cambiarValorDelTramo(control.origen));
            control.txtOrigen.on("blur", metodo.borrarValorDelTramo(control.origen));

            control.txtDestino.on("typeahead:selected", metodo.cambiarValorDelTramo(control.destino));
            control.txtDestino.on("typeahead:autocompleted", metodo.cambiarValorDelTramo(control.destino));
            control.txtDestino.on("blur", metodo.borrarValorDelTramo(control.destino));

        },
        configurarDatepicker: function configurarDatepicker(){
            var confDatepicker = { format: "yyyy-mm-dd", autoclose: true, todayHighlight: true, language: 'es' };
            control.salida.datepicker(confDatepicker);
        },
        mostrarImagenCargando: function mostrarImagenCargando() {
            var opts = {
                lines: 11, length: 25, width: 10, radius: 30, corners: 1, rotate: 0, direction: 1, color: '#0F1F43',
                speed: 1, trail: 60, shadow: false, hwaccel: false, className: 'spinner', zIndex: 2e9, top: 'auto',
                left: 'auto'
            };
            var target = $('#spinner').get(0);
            new Spinner(opts).spin(target);
        },
        buscarSugerencia: function buscarSugerencia() {
            metodo.mostrarImagenCargando();
            control.buscarSugerencia.button('loading');
        },
        validarFormulario: function validarFormulario (){
            var ok = true;

            if (!control.origen.val() || !control.txtOrigen.val()){
                control.origen.parent().addClass("has-error");
                ok = false;
            }

            if (!control.destino.val() || !control.txtDestino.val()){
                 control.destino.parent().addClass("has-error");
                 ok = false;
            }

            var expresionFecha = /^(19|20)\d\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/;
            if (!control.salida.val() && !expresionFecha.test(control.salida.val())){
                control.salida.parent().parent().addClass("has-error");
                ok = false;
            }

            if (ok){
                metodo.mostrarImagenCargando();
                control.buscar.button('loading');
            }

            return ok;
        },
        configurarValidacion: function configurarValidacion(){
            control.busqueda.on("submit", metodo.validarFormulario);

            var onFocus =  function onFocus() {
                $('.form-group', control.busqueda).removeClass("has-error");
            };

            control.txtOrigen.on("focus", onFocus);
            control.txtDestino.on("focus", onFocus);
            control.salida.on("focus", onFocus);
        },
        configurarBloqueoPorBusqueda: function configurarBloqueoPorBusqueda(){
            control.buscarSugerencia.click(metodo.buscarSugerencia);
            control.buscarSugerencia.button('reset');
            control.buscar.button('reset');
        },
        init: function init() {
            metodo.configurarTypeahead();
            metodo.configurarDatepicker();
            metodo.configurarValidacion();
            metodo.configurarBloqueoPorBusqueda();
        }
    };

    metodo.init();
})(window.pasajes = window.pasajes ? window.pasajes : {});