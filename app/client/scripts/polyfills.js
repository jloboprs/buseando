if (typeof Object.create !== 'function') {
    Object.create = function(o, props) {
        function F() {}
        F.prototype = o;

        if (typeof(props) === "object") {
            for (prop in props) {
                if (props.hasOwnProperty((prop))) {
                    F[prop] = props[prop];
                }
            }
        }
        return new F();
    };
}

if (!Array.prototype.reduce) {
    Array.prototype.reduce = function(callback /*, initialValue*/) {
        'use strict';
        if (this == null) {
            throw new TypeError('Array.prototype.reduce called on null or undefined');
        }
        if (typeof callback !== 'function') {
            throw new TypeError(callback + ' is not a function');
        }
        var t = Object(this), len = t.length >>> 0, k = 0, value;
        if (arguments.length == 2) {
            value = arguments[1];
        } else {
            while (k < len && ! k in t) {
                k++;
            }
            if (k >= len) {
                throw new TypeError('Reduce of empty array with no initial value');
            }
            value = t[k++];
        }
        for (; k < len; k++) {
            if (k in t) {
                value = callback(value, t[k], k, t);
            }
        }
        return value;
    };
}

if (!window.util) { window.util = {}; }
if (!util.format) {
    util.format = function format () {
        var args = [].slice.call(arguments);
        var initial = args.shift();

        function replacer (text, replacement) {
            return text.replace('%s', replacement);
        }
        return args.reduce(replacer, initial);
    };
}