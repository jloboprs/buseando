'use strict';

var rootRout = require('./rootRout'),
    pasajesRout = require('./pasajesRout'),
    asientoRout = require('./asientoRout'),
    apiRout = require('./apiRout'),
    estaticaRout = require('./estaticaRout');

module.exports = [ rootRout, pasajesRout, asientoRout,apiRout, estaticaRout ];