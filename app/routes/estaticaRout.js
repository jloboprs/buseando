'use strict';

var express = require('express');
var path = require('path');

var router = express.Router();

router.use('/robots.txt', express.static(path.join(__dirname, '../robots.txt')));
router.use('/sitemap.xml', express.static(path.join(__dirname, '../sitemap.xml')));
router.use('/sitemap.xml.gz', express.static(path.join(__dirname, '../sitemap.xml.gz')));
router.use('/backgroundsize.min.htc', express.static(path.join(__dirname, '../lib/backgroundsize.min.htc')));

router.use('/quienes-somos', function (req, res) { res.render('quienes-somos'); });

module.exports = router;