'use strict';

var express = require('express');
var apiController = require('../controllers/apiController');

var router = express.Router();

router.get('/api/lugares', apiController.enviarLugares);

module.exports = router;