'use strict';

var express = require('express');
var pasajesController = require('../controllers/pasajesController');

var router = express.Router();

var funcionesDePasajes = [ pasajesController.obtenerContexto, pasajesController.generarTramo,
    pasajesController.comprimirTramo, pasajesController.generarSugerencias, pasajesController.buscarPasajes,
    pasajesController.comprimirPasajes, pasajesController.generarResumenPasajes,
    pasajesController.renderizarPasajes, pasajesController.tratarError ];

router.get('/pasajes/:origen/:destino/:salida', pasajesController.botRedirect, funcionesDePasajes);
router.get('/pasajes/:origen/:destino/', funcionesDePasajes);
router.get('/pasajes', funcionesDePasajes);

module.exports = router;