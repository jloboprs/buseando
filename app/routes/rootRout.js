'use strict';

var express = require('express');
var rootController = require('../controllers/rootController');

var router = express.Router();

router.get('/', [rootController.obtenerGeo, rootController.generarSugerencia, rootController.enviarPaginaPrincipal]);

module.exports = router;
