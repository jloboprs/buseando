'use strict';

var express = require('express');
var asientoController = require('../controllers/asientoController');

var router = express.Router();

router.get('/asientos/:tramo/:pasaje',
    [ asientoController.obtenerParametros, asientoController.obtenerAsientos,
        asientoController.renderizarAsientos ]);

module.exports = router;