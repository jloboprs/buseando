'use strict';

var _ = require('underscore'),
    moment = require('moment'),
    utils = require('../lib/utils'),
    fabrica = require('nodos/fabrica'),
    repositorio = require('nodos/repositorio');

var obtenerBusquedasfrecuentes = function obtenerBusquedasfrecuentes(tramo){
    var destinosFrecuentes = ['aguachica','armenia','barrancabermeja','barranquilla','bogota','bosconia','bucaramanga','buga','cali','cartagena','cerete','chinu','cienaga','cienaga_de_oro','codazzi','cucuta','curumani','el_banco','honda','ibague','la_dorada','maicao','medellin','monteria'];

    return _.chain(repositorio.obtenerLugares())
        .filter(function(lugar){
            return _(destinosFrecuentes).contains(lugar.codigo);
        })
        .map(function(lugar){
            var tramoFrecuente  = { origen: tramo.origen, destino: lugar, salida: tramo.salida };

            return fabrica.tramo(tramoFrecuente);
        })
        .value();
};

var obtenerFinDeSemana = function obtenerFinDeSemana() {
    var viernes = 5;
    var fecha = new Date();
    var diferencia = viernes - fecha.getDay();

    var adicion = diferencia < 0
        ? 7 + diferencia
        : diferencia;

    return moment(fecha).add(adicion, 'days').toDate();
};

var obtenerTramoRandom = function obtenerTramoRandom(){
    var destino = _.chain(repositorio.obtenerLugares())
        .filter(function(destino){return destino.nombre.length < 13;})
        .sample()
        .value();

    var tramo  = {
        origen: { codigo:'bogota' },
        destino: destino,
        salida: obtenerFinDeSemana()
    };

    return fabrica.tramo(tramo);
};

module.exports = {
    obtenerBusquedasfrecuentes: obtenerBusquedasfrecuentes,
    obtenerTramoRandom: obtenerTramoRandom,
    obtenerFinDeSemana: obtenerFinDeSemana
};