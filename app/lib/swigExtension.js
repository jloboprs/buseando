'use strict';

var under = require('underscore'),
    numeral = require('numeral'),
    utils = require('./utils');

var filtroNumeral = function filtroNumeral(numero, formato) {
    formato = formato || '0,0';

    return numeral(numero).format(formato);
};

var filtroTransponer = function filtroTransponer(matriz){
    return under.zip.apply(under, (matriz));
};

var filtroCopia = function filtroCopia(objeto){
    return under.extend(objeto);
};

var aplicarFiltros = function setFilter(swig){
    swig.setFilter('numeral', filtroNumeral);
    swig.setFilter('trasponer', filtroTransponer);
    swig.setFilter('copia', filtroCopia);
};

module.exports = aplicarFiltros;