'use strict';

var q = require('q'),
    zlib = require('zlib'),
    moment = require('moment'),
    fabrica = require('nodos/fabrica'),
    repositorio = require('nodos/repositorio');

var homologarFecha = function homologarFecha(fecha) {
    fecha = moment(fecha, 'YYYY-MM-DD', true);

    return fecha.isValid() ? fecha.toDate() : undefined;
};

var homologarTramo = function homologarTramo(tramo) {
    var tramoHomologado = {
        origen: repositorio.obtenerLugar(tramo.origen),
        destino: repositorio.obtenerLugar(tramo.destino),
        salida: homologarFecha(tramo.salida),
        llegada: homologarFecha(tramo.llegada)
    };

    return fabrica.tramo(tramoHomologado);
};

var comprimir = function comprimir(objeto) {
    var defer = q.defer();
    var serializar = JSON.stringify(objeto);

    zlib.deflate(serializar, function (error, buffer) {
        if (error) {
            defer.reject(error);
        }

        defer.resolve(buffer.toString('base64'));
    });

    return defer.promise;
};

var descomprimir = function descomprimir(objeto) {
    var defer = q.defer();
    var buffer = new Buffer(objeto, 'base64');

    zlib.unzip(buffer, function (error, buffer) {
        if (error) {
            defer.reject(error);
        }
        defer.resolve(JSON.parse(buffer));
    });

    return defer.promise;
};

module.exports = {
    homologarTramo: homologarTramo,
    comprimir: comprimir,
    descomprimir: descomprimir
};