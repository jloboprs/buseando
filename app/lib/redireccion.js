var util = require('util');

var er = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(:\d{1,6})?$/;

var esIp = function esIP(host){
    return er.test(host);
};

var urlBase = function urlBase(protocol, host, url) {
    host = host.split('.').slice(-2).join('.');
    var urlBase = [protocol, '://', host];

    if (url) {
        urlBase.push(url);
    }

    return urlBase.join('');
};

var www = function www(req, res, next){
    var host = req.headers.host;

    if (esIp(host) || host.slice(0, 4) !== 'www.') {
        return next();
    }

    res.redirect(301, urlBase(req.protocol, host, req.url));
};

var subdominio = function subdominio(req, res, next){
    if (esIp(req.headers.host) || req.subdomains.length < 1){
        return next();
    }

    res.redirect(301, urlBase(req.protocol, req.headers.host));
};

module.exports = { www: www, subdominio: subdominio };