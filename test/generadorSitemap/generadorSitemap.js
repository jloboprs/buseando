'use strict';

var fs = require('fs'),
    util = require('util'),
    moment = require('moment'),
    under = require('underscore'),
    lugares = require('../../app/client/json/lugares.json'),
    bolivarianoConf = require('./bolivarianoConf.json'),
    copetranConf = require('./copetranConf.json'),
    brasiliaConf = require('./brasiliaConf.json');


var procesadorDestinos = function procesadorDestinos(stream, lista) {
    var fecha = moment().format(' YYYY-MM-DD');

    var url = function url(origen, destino) {
        return util.format('http://buseando.com/pasajes/%s/%s', origen.codigo, destino.codigo);
    };

    var esDiferente = function esDiferente(origen, destino){
        return origen.codigo !== destino.codigo;
    };

    var procesarDestino = function procesarDestino(origen, destino) {
        stream.write('<url><loc>');
        stream.write(procesador.url(origen, destino));
        stream.write('</loc><lastmod>');
        stream.write(fecha);
        stream.write('</lastmod><changefreq>weekly</changefreq><priority>0.7</priority></url>');
    };

    var procesar = function procesar(){
        under.each(lista, function(origen){
            under.each(lista, function(destino){
                if (procesador.esDiferente(origen, destino)){
                    procesador.procesarDestino(origen, destino);
                }
            });
        });
    };

    var procesador = {
        url: url,
        procesar: procesar,
        esDiferente: esDiferente,
        procesarDestino: procesarDestino,
        constructor: procesadorDestinos
    };

    return procesador;
};

var procesadorEmpresas = function Empresas(empresa, stream, configuracion){
    var destinos = under.keys(configuracion.ciudades);

    var url = function url(origen, destino) {
        return util.format('http://%s.buseando.com/pasajes/%s/%s', empresa, origen, destino);
    };

    var esDiferente = function esDiferente(origen, destino){
        return origen !== destino;
    };

    var procesador = {
        url: url,
        esDiferente: esDiferente,
        constructor: procesadorEmpresas
    };

    return under.extend(procesadorDestinos(stream, destinos), procesador);
};

var init = function init() {
    var stream = fs.createWriteStream("../app/sitemap.xml");

    stream.write('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');

    procesadorDestinos(stream, lugares).procesar();
    procesadorEmpresas('bolivariano', stream, bolivarianoConf).procesar();
    procesadorEmpresas('brasilia', stream, brasiliaConf).procesar();
    procesadorEmpresas('copetran', stream, copetranConf).procesar();

    stream.write('</urlset>');
    stream.end();
};

init();
