var _ = require('underscore');

var pasajeBase = function pasajeBase(valores){
    var pasaje;

    var precio = function precio(){
        return pasaje.tarifaBase || 0;
    };

    pasaje = {
        tarifaBase: undefined,
        servicio: undefined,
        operador: undefined,
        salida: new Date(),
        constructor: pasajeBase,
        precio: precio
    };

    return _.defaults(pasaje, valores);
};

var pasajeBolivariano = function pasajeBolivariano(valores){
    var pasaje;

    var precio = function precio(){
        return pasaje.tarifaBase || 0;
    };

    pasaje = {
        tarifaBase: 3,
        salida: new Date(),
        constructor: pasajeBolivariano,
        precio: precio
    };

    pasaje = _.defaults(pasaje, valores);
    return _.extend(pasajeBase(), pasaje);
};

var parametro = {servicio: "jaja"};
var tmo = pasajeBolivariano(parametro);

console.log(tmo);

return null;
asignarMetodosPasaje = function asignarMetodosPasaje(pasaje) {
    pasaje.precio = function precio() {
        var precios = [pasaje.tarifaIncreible, pasaje.tarifaChevere, pasaje.tarifaPrimeraClase];

        return under(precios).reduce(function (economico, precio) {
            return precio && precio < economico ? precio : economico;
        }, pasaje.tarifaComoda);
    };

    return pasaje;
};